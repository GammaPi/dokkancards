package com.dcv.spdesigns.dokkancards.model.filter_dialog.glb;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.model.glb.GlobalDataHolder;
import com.squareup.picasso.Picasso;

public class GlobalSsrImageAdapter extends BaseAdapter {

    private Context mContext;

    public GlobalSsrImageAdapter(Context c) {
        mContext = c;
    }

    @Override
    public int getCount() {
        return GlobalDataHolder.SSRCards.size();
    }

    @Override
    public Object getItem(int position) {
        return GlobalDataHolder.SSRCards.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        // If it's not recycled, initialize some attributes
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }
        //imageView.setImageResource(GlobalDataHolder.SSRCards.get(position).getCardIcon());
        Picasso.get().load(GlobalDataHolder.SSRCards.get(position).getCardIcon()).placeholder(R.drawable.placeholder).into(imageView);
        return imageView;
    }

    /**
     * Refreshes the fragment's view to display data changes
     * @param adapter Used to get the current instance of the fragment's adapter
     */
    public static void refreshFragmentView(GlobalSsrImageAdapter adapter) {
        adapter.notifyDataSetChanged();
    }
}
