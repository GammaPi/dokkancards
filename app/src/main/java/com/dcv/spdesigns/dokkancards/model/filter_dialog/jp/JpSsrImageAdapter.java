package com.dcv.spdesigns.dokkancards.model.filter_dialog.jp;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.model.jp.JPDataHolder;
import com.squareup.picasso.Picasso;

public class JpSsrImageAdapter extends BaseAdapter {

    private Context mContext;

    public JpSsrImageAdapter(Context c) {
        mContext = c;
    }

    @Override
    public int getCount() {
        return JPDataHolder.SSRCards.size();
    }

    @Override
    public Object getItem(int position) {
        return JPDataHolder.SSRCards.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        // If it's not recycled, initialize some attributes
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }
        //imageView.setImageResource(JPDataHolder.SSRCards.get(position).getCardIcon());
        Picasso.get().load(JPDataHolder.SSRCards.get(position).getCardIcon()).placeholder(R.drawable.placeholder).into(imageView);
        return imageView;
    }
}
