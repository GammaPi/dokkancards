package com.dcv.spdesigns.dokkancards.model.filter_dialog.main;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.model.main.CardInfoDatabase;
import com.dcv.spdesigns.dokkancards.presenter.MainActivity;
import com.dcv.spdesigns.dokkancards.presenter.NewCardViewActivity;
import com.squareup.picasso.Picasso;

public class CategoryLeadersRecyclerviewAdapter extends RecyclerView.Adapter<CategoryLeadersRecyclerviewAdapter.ViewHolder> {

private Context mContext;
private static int filterOptionSelected;
private LayoutInflater mInflater;

public CategoryLeadersRecyclerviewAdapter(Context c) {
        mContext = c;
        }

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    ImageView imageView;

    ViewHolder(View v) {
        super(v);
        imageView = v.findViewById(R.id.main_img_item);
        imageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.i("tAG", "Card number [Category Lead]:  " + getLayoutPosition());
    }
}

    // inflates the cell layout form xml when needed
    @NonNull
    @Override
    public CategoryLeadersRecyclerviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mInflater = (LayoutInflater) mContext.getSystemService(MainActivity.LAYOUT_INFLATER_SERVICE);
        View rootView = mInflater.inflate(R.layout.main_recyclerview_item,parent,false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        return new CategoryLeadersRecyclerviewAdapter.ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        Picasso.get().load(CardInfoDatabase.CategoryLeaders[position].getCardIcon()).placeholder(R.drawable.placeholder).into(viewHolder.imageView);
        viewHolder.imageView.setOnClickListener(view -> {
            Intent testCardView = new Intent(mContext, NewCardViewActivity.class);
            testCardView.putExtra("Card Index", position);
            testCardView.putExtra("Identifier", 0);
            testCardView.putExtra("filterOption", filterOptionSelected);
            mContext.startActivity(testCardView);
        });
    }

    @Override
    public int getItemCount() {
        return CardInfoDatabase.CategoryLeaders.length;
    }

    public static void setFilterDialogOptionSelected(int filterOption) {
        filterOptionSelected = filterOption;
        Log.i("TAG","FilterOption:" + filterOptionSelected);
    }
}
