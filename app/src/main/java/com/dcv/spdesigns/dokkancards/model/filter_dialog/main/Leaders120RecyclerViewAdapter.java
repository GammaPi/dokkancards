package com.dcv.spdesigns.dokkancards.model.filter_dialog.main;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.model.main.CardInfoDatabase;
import com.dcv.spdesigns.dokkancards.presenter.MainActivity;
import com.dcv.spdesigns.dokkancards.presenter.NewCardViewActivity;
import com.squareup.picasso.Picasso;

/**
 * DokkanCards was
 * Created by Stelios Papamichail on 8/18/2018.
 * <p>
 * This file belongs to the com.dcv.spdesigns.dokkancards.model.filter_dialog.main package.
 */
public class Leaders120RecyclerViewAdapter extends RecyclerView.Adapter<Leaders120RecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private static int filterOptionSelected;
    private LayoutInflater mInflater;

    public Leaders120RecyclerViewAdapter(Context c) {
        mContext = c;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imageView;

        ViewHolder(View v) {
            super(v);
            imageView = v.findViewById(R.id.main_img_item);
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.i("tAG", "Card number [120 Lead]:  " + getLayoutPosition());
        }
    }

    // inflates the cell layout form xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mInflater = (LayoutInflater) mContext.getSystemService(MainActivity.LAYOUT_INFLATER_SERVICE);
        View rootView = mInflater.inflate(R.layout.main_recyclerview_item,parent,false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        Picasso.get().load(CardInfoDatabase.Leaders120[position].getCardIcon()).placeholder(R.drawable.placeholder).into(viewHolder.imageView);
        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent testCardView = new Intent(mContext, NewCardViewActivity.class);
                testCardView.putExtra("Card Index", position);
                testCardView.putExtra("Identifier", 0);
                testCardView.putExtra("filterOption", filterOptionSelected);
                mContext.startActivity(testCardView);
            }
        });
    }

    @Override
    public int getItemCount() {
        return CardInfoDatabase.Leaders120.length;
    }

    public static void setFilterDialogOptionSelected(int filterOption) {
        filterOptionSelected = filterOption;
        Log.i("TAG","FilterOption:" + filterOptionSelected);
    }
}
