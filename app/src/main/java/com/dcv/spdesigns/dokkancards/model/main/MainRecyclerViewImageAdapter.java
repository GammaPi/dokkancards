package com.dcv.spdesigns.dokkancards.model.main;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.presenter.MainActivity;
import com.dcv.spdesigns.dokkancards.presenter.NewCardViewActivity;
import com.dcv.spdesigns.dokkancards.ui.MainScreenFragmentV2;
import com.squareup.picasso.Picasso;

public class MainRecyclerViewImageAdapter extends android.support.v7.widget.RecyclerView.Adapter<MainRecyclerViewImageAdapter.ViewHolder> {

    private static int filterOptionSelected;
    private LayoutInflater mInflater;
    private Context mContext;

    public MainRecyclerViewImageAdapter(Context c) {
        mContext = c;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnCreateContextMenuListener{
        ImageView imageView;

        ViewHolder(View v) {
            super(v);
            imageView = v.findViewById(R.id.main_img_item);
            imageView.setOnClickListener(this);
            imageView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.i("tAG", "Card number:  " + getLayoutPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            if(MainScreenFragmentV2.canAddCardToBox()) {
                menu.setHeaderTitle("Card Options");
                //AdapterView.AdapterContextMenuInfo cmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
                menu.add(this.getAdapterPosition(), view.getId(), 0, "Add Card to GLB");
                menu.add(this.getAdapterPosition(), view.getId(), 0, "Add Card to JP");
            }
        }
    }

    // inflates the cell layout form xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mInflater = (LayoutInflater) mContext.getSystemService(MainActivity.LAYOUT_INFLATER_SERVICE);
        View rootView = mInflater.inflate(R.layout.main_recyclerview_item,parent,false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        return new ViewHolder(rootView);
    }

    // binds the data to the imageview in each cell
    @Override
    public void onBindViewHolder(@NonNull MainRecyclerViewImageAdapter.ViewHolder viewHolder, final int position) {
        Picasso.get().load(CardInfoDatabase.cardDatabase[position].getCardIcon()).placeholder(R.drawable.placeholder).into(viewHolder.imageView);
        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent testCardView = new Intent(mContext, NewCardViewActivity.class);
                testCardView.putExtra("Card Index",position);
                testCardView.putExtra("Identifier", 0);
                testCardView.putExtra("filterOption",filterOptionSelected);
                mContext.startActivity(testCardView);
            }
        });
    }

    @Override
    public int getItemCount() {
        return CardInfoDatabase.cardDatabase.length;
    }

    public static void setFilterDialogOptionSelected(int filterOption) {
        filterOptionSelected = filterOption;
        Log.i("TAG","FilterOption:" + filterOptionSelected);
    }
}
