package com.dcv.spdesigns.dokkancards.presenter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.dcv.spdesigns.dokkancards.R;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

import com.google.android.gms.oss.licenses.OssLicensesMenuActivity;

/**
 * An Activity class that uses a Third Party Library to create
 * the app's "About Page".
 */

public class AboutPageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Element Twitter = new Element();
        Twitter.setTitle("Find me on Twitter");
        Twitter.setIconDrawable(R.drawable.twitter_icon_96dp);
        Twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String twitterUrl = "https://twitter.com/MikePapamichail";
                Intent twitterIntent = new Intent(Intent.ACTION_VIEW);
                twitterIntent.setData(Uri.parse(twitterUrl));
                startActivity(twitterIntent);
            }
        });

        Element Contributors = new Element();
        Contributors.setTitle("Contributors");
        Contributors.setIconDrawable(R.mipmap.ic_people_black_24dp);
        Intent contributorsIntent = new Intent(this,ContributorsActivity.class);
        Contributors.setIntent(contributorsIntent);

        Element Donators = new Element();
        Donators.setTitle("Donators");
        Donators.setIconDrawable(R.drawable.baseline_face_white_24);
        Intent donatorsIntent = new Intent(this, com.dcv.spdesigns.dokkancards.presenter.Donators.class);
        Donators.setIntent(donatorsIntent);

        Element version = new Element();
        version.setTitle("Version 1.3");

        Element donations = new Element();
        donations.setTitle("Support Us");
        donations.setIconDrawable(R.mipmap.icons8_heart);
        donations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String donationsUrl = "https://paypal.me/SteliosPapamichail";
                Intent donationsIntent = new Intent(Intent.ACTION_VIEW);
                donationsIntent.setData(Uri.parse(donationsUrl));
                startActivity(donationsIntent);
            }
        });

        Element Licenses = new Element();
        Licenses.setTitle("Open Source Licenses");
        Licenses.setIconDrawable(R.mipmap.ic_assignment_late_black_24dp);
        Intent licensesIntent = new Intent(this, OssLicensesMenuActivity.class);
        String title = "Open Source Licenses";
        licensesIntent.putExtra("title", title);
        Licenses.setIntent(licensesIntent);

        Element disclaimer = new Element();
        disclaimer.setTitle("Copyright Disclaimer");
        disclaimer.setIconDrawable(R.mipmap.ic_content_paste_black_24dp);
        Intent disclaimerIntent = new Intent(this,DisclaimerActivity.class);
        disclaimer.setIntent(disclaimerIntent);

        Element ThirdPartyLibs = new Element();
        ThirdPartyLibs.setTitle("Third Party Libraries");
        ThirdPartyLibs.setIconDrawable(R.mipmap.ic_library_books_black_24dp);
        Intent thirdPartyLibsIntent = new Intent(this,ThirdPartyLibsActivity.class);
        ThirdPartyLibs.setIntent(thirdPartyLibsIntent);


        View aboutPage = new AboutPage(this)
                .isRTL(false)
                .setDescription("Dokkan Cards is an application developed by \n Stelios Papamichail for the community of \n Dragon Ball Z Dokkan Battle.\n\n This app was made in Greece")
                .setImage(R.drawable.ic_launcher)
                .addGroup("Connect with me")
                .addEmail("stelios_papamichael@hotmail.com","Personal Email")
                .addItem(Twitter)
                .addItem(donations)
                .addGroup("Important Info")
                .addItem(Contributors)
                .addItem(Donators)
                .addItem(ThirdPartyLibs)
                .addItem(Licenses)
                .addItem(disclaimer)
                .addGroup("Version Info")
                .addItem(version)
                .create();
        setContentView(aboutPage);
    }
}
