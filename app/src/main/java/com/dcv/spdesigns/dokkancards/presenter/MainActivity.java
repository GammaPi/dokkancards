package com.dcv.spdesigns.dokkancards.presenter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ajithvgiri.searchdialog.OnSearchItemSelected;
import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.dcv.spdesigns.dokkancards.BuildConfig;
import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.model.glb.GlobalDataHolder;
import com.dcv.spdesigns.dokkancards.model.glb.SerializeGLBData;
import com.dcv.spdesigns.dokkancards.model.jp.JPDataHolder;
import com.dcv.spdesigns.dokkancards.model.jp.SerializeJPData;
import com.dcv.spdesigns.dokkancards.model.main.CardInfoDatabase;
import com.dcv.spdesigns.dokkancards.ui.MainScreenFragmentV2;
import com.dcv.spdesigns.dokkancards.ui.PermissionExplanationDialog;
import com.dcv.spdesigns.dokkancards.ui.SortingDialog;
import com.dcv.spdesigns.dokkancards.ui.Tutorial;
import com.dcv.spdesigns.dokkancards.ui.TutorialOptionsMenu;
import com.dcv.spdesigns.dokkancards.ui.UserBoxGLBFragment;
import com.dcv.spdesigns.dokkancards.ui.UserBoxJPFragment;
import com.dcv.spdesigns.dokkancards.ui.UsernameDialog;
//import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The main Activity class that handles most of the app's code!
 * Definitely the most important .java file in this project LOL.
 */
public class MainActivity extends AppCompatActivity {

    // Firebase Analytics object instance
    //private FirebaseAnalytics mFirebaseAnalytics;

    private final String TAG = this.getClass().getSimpleName();

    final String PREFS_NAME = "DokkanCardsPrefs"; // SHAREDPREFS NAME

    private TextView usernameTextView;

    // NavMenu member vars
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle; // Button for toggling the side menu

    // Keeps the position of the previously selected menu item(0 : Home)
    private final int position = 0;

    // App permisions
    private final String[] permissions = {Manifest.permission.INTERNET,Manifest.permission.WAKE_LOCK,Manifest.permission.ACCESS_NETWORK_STATE,Manifest.permission.ACCESS_WIFI_STATE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //mFirebaseAnalytics = FirebaseAnalytics.getInstance(this); // obtain the firebaseAnalytics instance

        // Check if the app has the required permissions to work properly
//        boolean arePermsGranted;
//        if(isUserPlatformLollipopOrLess()) {
//            arePermsGranted = true;
//        } else {
//            arePermsGranted = promptForPermissions();
//        }
            usernameTextView = findViewById(R.id.username);

            checkForFirstRun();
            callReadDataMethodsGLB(this);
            callReadDataMethodsJP(this);
            initRarityListsGLB();
            initRarityListsJP();
            //Log.i("USERNAME",getSharedPreferences(PREFS_NAME,MODE_PRIVATE).getString("Username","error"));

            mDrawerLayout = findViewById(R.id.drawerLayout);
            NavigationView navigationView = findViewById(R.id.nav_view);

            changeUsername(navigationView);

            mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_closed); // Instantiating our button

            final Toolbar mToolbar = findViewById(R.id.navActionBar);
            setSupportActionBar(mToolbar); // check quick doq
            try {
                //noinspection ConstantConditions
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            } catch (NullPointerException npe) {
                npe.printStackTrace();
            }
            mToolbar.setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setOverflowIcon(getDrawable(R.mipmap.ic_more_vert_white_24dp));

            // Sets the default selected menu item, to the Home item
            navigationView.getMenu().findItem(R.id.nav_home).setChecked(true);

            // Used to help on check and uncheck menu items when the user clicks on them
            final List<MenuItem> items = new ArrayList<>();
            Menu menu;
            menu = navigationView.getMenu();

            // Fill the list with all the menu items
            for (int i = 0; i < menu.size(); i++) {
                items.add(menu.getItem(i));
            }

            // Set the default starting screen to the mainScreen
            FragmentManager startingScreenManager = getSupportFragmentManager();
            FragmentTransaction startingScreenTransaction = startingScreenManager.beginTransaction();

            final MainScreenFragmentV2 fragmentV2 = new MainScreenFragmentV2();
            startingScreenTransaction.add(R.id.FrameLayoutContainer,fragmentV2);
            startingScreenTransaction.commit();

            // When an item inside the NavView gets clicked, then handle the event...
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

                @SuppressLint({"ResourceAsColor", "ResourceType"})
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    // Initializing these vars again for use in this inner class
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    // Replace the main Fragment in this activity based on the menu item selected
                    switch (item.getItemId()) {
                        case R.id.nav_home:
                            changeActionBarColor(1);

                            //TODO:sp used to send user data to the firebase console
//                            Bundle bundle = new Bundle();
//                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID,"0");
//                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME,"Database");
//                            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE,"Menu Option");
                           // mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT,bundle);

                            MainScreenFragmentV2 mainScreenFragmentV2 = new MainScreenFragmentV2();
                            fragmentTransaction.replace(R.id.FrameLayoutContainer, mainScreenFragmentV2, "MainFragment");
                            fragmentTransaction.commit();
                            break;
                        case R.id.nav_UserBoxGLB:
                            changeActionBarColor(2);
                            // Initializing the globalUserBox Fragment
                            initRarityListsGLB();
                            UserBoxGLBFragment glbFragment = new UserBoxGLBFragment();
                            fragmentTransaction.replace(R.id.FrameLayoutContainer, glbFragment, "GLOBAL_FRAGMENT");
                            fragmentTransaction.commit();
                            break;
                        case R.id.nav_UserBoxJP:
                            changeActionBarColor(3);
                            // Initializing the jpUserBox fragment
                            initRarityListsJP();
                            UserBoxJPFragment jpFragment = new UserBoxJPFragment();
                            fragmentTransaction.replace(R.id.FrameLayoutContainer, jpFragment, "JP_FRAGMENT");
                            fragmentTransaction.commit();
                            break;
                        case R.id.nav_supportus:
                            Intent donateIntent = new Intent(Intent.ACTION_VIEW);
                            donateIntent.setData(Uri.parse("https://paypal.me/SteliosPapamichail"));
                            startActivity(donateIntent);
                            break;
                        case R.id.nav_github:
                            Intent gitRepo = new Intent(Intent.ACTION_VIEW);
                            gitRepo.setData(Uri.parse("https://bitbucket.org/GammaPi/dokkancards/src/master/"));
                            startActivity(gitRepo);
                            break;
                        case R.id.nav_feedback:
                            composeEmail(supportEmail, "[Beta] Feedback");
                            break;
                        case R.id.nav_contact_us:
                            composeEmail(emails, "Contact Us");
                            break;
                        case R.id.nav_rate:
                            rateApp();
                            break;
                        case R.id.nav_about:
                            Intent aboutIntent = new Intent(MainActivity.this, AboutPageActivity.class);
                            startActivity(aboutIntent);
                            break;
                        default:
                            return onNavigationItemSelected(item);
                    }
                    items.get(position).setChecked(false);
                    item.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return false;
                }
            });

            mDrawerLayout.addDrawerListener(mToggle);
            // Set the hamburger icon's color
            mToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.NavActionBarTextColor));
            mToggle.syncState();
    }

    /**
     * Accesses the textView located inside the navHeader and
     * changes the textView's text value to the new selected username
     * @param navigationView The navView used to access the textView from.
     */
    private void changeUsername(NavigationView navigationView) {
        View drawerHead = navigationView.getHeaderView(0);
        usernameTextView = drawerHead.findViewById(R.id.username);
        usernameTextView.setText(applyUsername());
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Write all the data retrieved from BOTH GlobalDataHolder & JPDataHolder to the internal memory
        SerializeGLBData.Write(GlobalDataHolder.cards,this);
        SerializeJPData.Write(JPDataHolder.cards,this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu,menu);
        return true;
    }

    // When an item from the Action Bar gets tapped, then...
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options_tutorial:
                Intent tutorialIntent = new Intent(this,TutorialOptionsMenu.class);
                startActivity(tutorialIntent);
                return true;
            case R.id.options_username:
                showUsernameDialog();
                return true;
            case R.id.options_help:
                composeEmail(supportEmail,"Dokkan Cards : [Subject here]");
                return true;
            case R.id.options_sorting:
                showSortingDialog();
                return true;
            case R.id.options_search:
                List<SearchListItem> searchListItems = new ArrayList<>();
                determineSearchDataSet(searchListItems);
                return true;
        }
        return mToggle.onOptionsItemSelected(item) || onOptionsItemSelected(item);
    }

    private void showUsernameDialog() {
        UsernameDialog dialog = new UsernameDialog();
        dialog.show(getFragmentManager(),"USERNAME_DIALOG");
    }

    /**
     * Determines with which data to initialize the search dialog.
     * The data set selected is based on the currently displayed Fragment.
     * @param searchListItems The data set used to initialize the search dialog.
     */
    private void determineSearchDataSet(List<SearchListItem> searchListItems) {
        int id = -1;
        android.support.v4.app.Fragment currentF = getSupportFragmentManager().findFragmentById(R.id.FrameLayoutContainer);
        if(currentF instanceof MainScreenFragmentV2) {
            for (int i = 0; i < CardInfoDatabase.cardDatabase.length; i++) {
                searchListItems.add(new SearchListItem(i, CardInfoDatabase.cardDatabase[i].getName()));
                id = 0;
            }
        } else if(currentF instanceof UserBoxGLBFragment) {
            for (int i = 0; i < GlobalDataHolder.cards.size(); i++) {
                searchListItems.add(new SearchListItem(i, GlobalDataHolder.cards.get(i).getName()));
                id = 1;
            }
        } else if(currentF instanceof UserBoxJPFragment) {
            for (int i = 0; i < JPDataHolder.cards.size(); i++) {
                searchListItems.add(new SearchListItem(i, JPDataHolder.cards.get(i).getName()));
                id = 2;
            }
        } else {
            Log.d(TAG,"No valid Fragment found in MainActivity");
        }

        createSearchDialog(searchListItems,id);
    }

    /**
     * Create a dialog that can be used for searching cards based on their name and description.
     * The dialog's contents are initiated by the determineSearchDataSet() method.
     * @param searchListItems The data set determined by the determineSearchDataSet() method.
     * @param identifier An integer used to determine from within which fragment  the search dialog was instantiated.
     */
    private void createSearchDialog(List<SearchListItem> searchListItems,final int identifier) {
        SearchableDialog dialog = new SearchableDialog(this,searchListItems,"Search");
        dialog.show();
        dialog.setOnItemSelected(new OnSearchItemSelected() {
            @Override
            public void onClick(int i, SearchListItem searchListItem) {
                Intent searchResIntent = new Intent(MainActivity.this, NewCardViewActivity.class);
                searchResIntent.putExtra("Card Index", i);
                searchResIntent.putExtra("Identifier", identifier);
                searchResIntent.putExtra("filterOption", 0);
                startActivity(searchResIntent);
            }
        });
    }

    /**
     * Shows the sorting dialog in the current activity
     */
    private void showSortingDialog() {
        DialogFragment sortingDialog = new SortingDialog();
        sortingDialog.show(getSupportFragmentManager(),"Filter_Dialog");
    }

    private final String[] supportEmail = {"steliospapamichael17@gmail.com "};
    private final String[] emails = {"steliospapamichael17@gmail.com" };

    /**
     * Send an email to the @address with a @subject
     * @param addresses The email address(es) to send the email to
     * @param subject The email's subject
     */
    private void composeEmail(String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, "[Your message here]");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    /**
     * Calls every available Read method to retrieve all available data from the GLB database
     */
    public static void callReadDataMethodsGLB(Context context) {
        GlobalDataHolder.cards = SerializeGLBData.ReadCards(context);
        Log.i("Read Methods[GLB]", "ReadMethods called!");
    }

    /**
     * Calls every available Read method to retrieve all available data from the JP database
     */
    public static void callReadDataMethodsJP(Context context) {
        JPDataHolder.cards = SerializeJPData.ReadCards(context);
        Log.i("Read Methods[JP]", "ReadMethods called!");
    }

    private void checkForFirstRun() {

        final String PREF_VERSION_CODE_KEY = "version_code";
        final int DOESNT_EXIST = -1;

        // Get current version code
        int currentVersionCode = BuildConfig.VERSION_CODE;

        // Get saved version code
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        // Check for first run or upgrade
        if(currentVersionCode == savedVersionCode) {
            // This is just a normal run
            Log.d("RUN_TYPE:" , "Normal Run");
        } else if(savedVersionCode == DOESNT_EXIST) { // This is a new install(or the user cleared the shared prefs)
            CallWriteDataMethods(this);
            Log.d("RUN_TYPE:", "New Install");
            // Showing the tutorial page when the app starts for the first time
            Intent tutorialIntent = new Intent(this, Tutorial.class);
            startActivity(tutorialIntent);
            UsernameDialog dialog = new UsernameDialog();
            dialog.setCancelable(false);
            dialog.show(getFragmentManager(),"USERNAME_DIALOG");
        } else if(currentVersionCode > savedVersionCode) { // This is an upgrade
            callReadDataMethodsGLB(this);
            callReadDataMethodsJP(this);
            Log.d("RUN_TYPE:","Update");
        }

        // Update the shared prefs with the current version code
        prefs.edit().putInt(PREF_VERSION_CODE_KEY,currentVersionCode).apply();
        return;
    }

    public static void CallWriteDataMethods(Context context) {
        // Write all the -empty- data from GlobalDataHolder to the internal memory to avoid a first time read error
        SerializeGLBData.Write(GlobalDataHolder.cards,context);
        // Write all the -empty- data from JPDataHolder to the internal memory to avoid a first time read error
        SerializeJPData.Write(JPDataHolder.cards,context);
    }

    /**
     * Changes the ActionBar's color to match the currently selected fragment's pallet
     * @param menuItemPos Determines which fragment is currently being displayed
     */
    private void changeActionBarColor(int menuItemPos) {
        switch (menuItemPos) {
            case 1:
                //noinspection ConstantConditions
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
                break;
            case 2:
                //noinspection ConstantConditions
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryGLB)));
                break;
            case 3:
                //noinspection ConstantConditions
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryJP)));
                break;
        }
    }

    /**
     * Initializes the various rarity related
     * lists for the GLB Fragment dynamically.
     */
    public static void initRarityListsGLB() {
        clearRarityListsGLB();
        for (int i=0;i<GlobalDataHolder.cards.size();i++) {
            if(GlobalDataHolder.cards.get(i).getRarity().equals("LR")) {
                GlobalDataHolder.LRCards.add(GlobalDataHolder.cards.get(i));
            }
        }
        for (int i=0;i<GlobalDataHolder.cards.size();i++) {
            if(GlobalDataHolder.cards.get(i).getRarity().equals("UR")) {
                GlobalDataHolder.URCards.add(GlobalDataHolder.cards.get(i));
            }
        }
        for(int i=0;i<GlobalDataHolder.cards.size();i++) {
            if(GlobalDataHolder.cards.get(i).getRarity().equals("SSR")) {
                GlobalDataHolder.SSRCards.add(GlobalDataHolder.cards.get(i));
            }
        }
    }

    /**
     *Empties the rarity lists so that they won't duplicate upon being called multiple times
     */
    private static void clearRarityListsGLB() {
        GlobalDataHolder.LRCards.clear();
        GlobalDataHolder.URCards.clear();
        GlobalDataHolder.SSRCards.clear();
    }

    /**
     * Initializes the various rarity related
     * lists for the JP Fragment dynamically.
     */
    public static void initRarityListsJP() {
        clearRarityListsJP();
        for (int i = 0; i < JPDataHolder.cards.size(); i++) {
            if (JPDataHolder.cards.get(i).getRarity().equals("LR")) {
                JPDataHolder.LRCards.add(JPDataHolder.cards.get(i));
            }
        }
        for (int i = 0; i < JPDataHolder.cards.size(); i++) {
            if (JPDataHolder.cards.get(i).getRarity().equals("UR")) {
                JPDataHolder.URCards.add(JPDataHolder.cards.get(i));
            }
        }
        for(int i=0;i < JPDataHolder.cards.size();i++) {
            if(JPDataHolder.cards.get(i).getRarity().equals("SSR")) {
                JPDataHolder.SSRCards.add(JPDataHolder.cards.get(i));
            }
        }
    }

    /**
     * *Empties the rarity lists so that they won't duplicate upon being called multiple times
     */
    private static void clearRarityListsJP() {
        JPDataHolder.LRCards.clear();
        JPDataHolder.URCards.clear();
        JPDataHolder.SSRCards.clear();
    }

    /**
     * Checks the user's os version.If it's on Lollipop or earlier,
     * then return true since the app permissions were granted upon app download.Otherwise
     * return false.
     * @return The appropriate boolean value of the above condition
     */
    private boolean isUserPlatformLollipopOrLess() {
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if each of the app's permissions were granted
     * by the user at runtime.If they are not, request them explicitly.
     * If the user denies any one of the permissions,the method will return
     * false and the app will exit (since all of them are required for the app
     * to work properly).
     * @return Returns true if they were all granted and false if any of them was not.
     */
    private boolean promptForPermissions() {
        boolean permDenied = false;
        for(int i=0; i <= permissions.length; i++) {
            ActivityCompat.requestPermissions(this, new String[]{permissions[i]}, i);
            if(ContextCompat.checkSelfPermission(this,permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    // permission is not granted
                    // should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i])) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                        PermissionExplanationDialog permExplanationDialog = new PermissionExplanationDialog();
                        permExplanationDialog.setPermissionId(i);
                        permExplanationDialog.show(getFragmentManager(),"PERM_EXPL_DIALOG");
                    }
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(this, new String[]{permissions[i]}, i);

                    if(ContextCompat.checkSelfPermission(this,permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                        permDenied = true;
                        break;
                    }
            }
        }
        return !permDenied;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void rateApp() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // In order to get back to our app after pressing the back button, we need to add following flags to the intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }

    private String applyUsername() {
        return getSharedPreferences(PREFS_NAME,MODE_PRIVATE).getString("Username","[Username]");
    }
}
