package com.dcv.spdesigns.dokkancards.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.model.glb.GlobalDataHolder;
import com.dcv.spdesigns.dokkancards.model.jp.JPDataHolder;
import com.dcv.spdesigns.dokkancards.model.main.CardInfoDatabase;
import com.dcv.spdesigns.dokkancards.ui.CardDetailsDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * This class binds the model to the view with the
 * right Card data.
 */
public class NewCardViewActivity extends AppCompatActivity {

    private final String TAG = NewCardViewActivity.class.getSimpleName();
    private String leader_skill_text = "";
    private String  super_attack_text="";
    private String  passive_skill_text="";
    private String leaderDialogText = "";
    private String superDialogText = "";
    private String passiveDialogText = "";
    private int cardIndex;
    private int sourceIdentifier;
    private int filterOption;
    private int filterOptionGLB;
    private int filterOptionJP;

    @BindView(R.id.rarity) ImageView rarityImage;
    @BindView(R.id.type) ImageView typeImage;
    @BindView(R.id.nameBtn) Button nameButton;
    @BindView(R.id.arrow_button) ImageView arrowButton;
    @BindView(R.id.fullDetails) ConstraintLayout constraintLayout;
    @BindView(R.id.constraintLayout_image) ConstraintLayout backgroundImage;
    @BindView(R.id.HP) TextView hp;
    @BindView(R.id.ATT) TextView att;
    @BindView(R.id.DEF) TextView def;
    @BindView(R.id.COST) TextView cost;
    @BindView(R.id.leader_skill_text) TextView leaderSkill;
    @BindView(R.id.passive_skill) TextView passiveSkill;
    @BindView(R.id.super_attack) TextView superAttack;
    @BindView(R.id.ls1) TextView linkSkill1;
    @BindView(R.id.ls2) TextView linkSkill2;
    @BindView(R.id.ls3) TextView linkSkill3;
    @BindView(R.id.ls4) TextView linkSkill4;
    @BindView(R.id.ls5) TextView linkSkill5;
    @BindView(R.id.ls6) TextView linkSkill6;
    @BindView(R.id.ls7) TextView linkSkill7;
    @OnClick(R.id.arrow_button) void arrowListener() {
        if (constraintLayout.getVisibility() == View.VISIBLE) {
            arrowButton.setBackground(getResources().getDrawable(R.drawable.arrow_up));
            constraintLayout.setVisibility(View.GONE);
        } else {
            arrowButton.setBackground(getResources().getDrawable(R.drawable.arrow_down));
            constraintLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_details);
        ButterKnife.bind(this);
        nameButton.setBackgroundResource(R.drawable.menu_icon);

        leader_skill_text = leaderSkill.getText().toString();
        super_attack_text = superAttack.getText().toString();
        passive_skill_text = passiveSkill.getText().toString();

        Intent source = getIntent();
        Bundle bundle = source.getExtras();

        if(bundle != null) {
            cardIndex = bundle.getInt("Card Index");
            sourceIdentifier = bundle.getInt("Identifier");
            filterOption = bundle.getInt("filterOption");
            filterOptionGLB = bundle.getInt("filterOptionGLB");
            filterOptionJP = bundle.getInt("filterOptionJP");
        } else {
            Log.d(TAG,"Error getting bundle extras!");
        }

        callInitDataMethods();


        determineTextLength();

        leaderSkill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardDetailsDialog dialog = new CardDetailsDialog();
                dialog.setTextToDisplay(leaderDialogText);
                dialog.show(getFragmentManager(),"Details Dialog");
            }
        });

        superAttack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardDetailsDialog dialog = new CardDetailsDialog();
                dialog.setTextToDisplay(superDialogText);
                dialog.show(getFragmentManager(),"Details Dialog");
            }
        });

        passiveSkill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardDetailsDialog dialog = new CardDetailsDialog();
                dialog.setTextToDisplay(passiveDialogText);
                dialog.show(getFragmentManager(),"Details Dialog");
            }
        });
    }

    /**
     * Determines if the super_attack_text,leader_skill_texr or
     * passive_skill_text Strings are too long to display
     * in a single line.If that's the case, some adjustments are
     * made.
     */
    private void determineTextLength() {

        if(leader_skill_text.length() > 40) {
            leader_skill_text = leader_skill_text.substring(0,41)+"...";
            leaderSkill.setText(Html.fromHtml(leader_skill_text+"<font color='green'> <u>details</u></font>"));
        }

        if (super_attack_text.length()>40) {
            super_attack_text=super_attack_text.substring(0,41)+"...";
            superAttack.setText(Html.fromHtml(super_attack_text+"<font color='green'> <u>details</u></font>"));

        }

        if (passive_skill_text.length()>40) {
            passive_skill_text=passive_skill_text.substring(0,41)+"...";
            passiveSkill.setText(Html.fromHtml(passive_skill_text+"<font color='green'> <u>details</u></font>"));

        }
    }

    /**
     * Calls the appropriate initCardView() method based on the sourceIdentifier field's value
     */
    private void callInitDataMethods() {
        if(sourceIdentifier == 0) {
            initCardViewMainScreen(cardIndex,filterOption);
        } else if(sourceIdentifier == 1) {
            initCardViewGLB(cardIndex,filterOptionGLB);
        } else if(sourceIdentifier == 2) {
            initCardViewJP(cardIndex,filterOptionJP);
        } else {
            Log.d(TAG, "Error initializing card view data");
        }
    }

    /**
     * Determines the rarity of the selected card
     * and sets the respective image's resource drawable
     * accordingly.
     * @param selectedCardRarity The selected card's rarity
     */
    private void determineRarity(String selectedCardRarity) {
        if(selectedCardRarity.equals("LR")) {
            rarityImage.setImageResource(R.drawable.lr_rarity_icon);
        } else if(selectedCardRarity.equals("UR")) {
            rarityImage.setImageResource(R.drawable.ur_icon);
        } else if(selectedCardRarity.equals("SSR")) {
            rarityImage.setImageResource(R.drawable.ssr_icon);
        } else {
            Log.d(TAG,"Error determining rarity!");
        }
    }

    /**
     * Determines the card's type and sets the respective
     * image's resource drawable to the appropriate icon
     * @param selectedCardType The selected card's type
     */
    private void determineType(String selectedCardType) {
        if(selectedCardType.equals("PHY")) {
            typeImage.setImageResource(R.drawable.phy_icon_200x200);
        } else if(selectedCardType.equals("INT")) {
            typeImage.setImageResource(R.drawable.int_icon_200x200);
        } else if(selectedCardType.equals("AGL")) {
            typeImage.setImageResource(R.drawable.agl_icon_200x200);
        } else if(selectedCardType.equals("STR")) {
            typeImage.setImageResource(R.drawable.str_icon_200x200);
        } else if(selectedCardType.equals("TEQ")) {
            typeImage.setImageResource(R.drawable.teq_icon_200x200);
        } else if(selectedCardType.equals("Super PHY")) {
            typeImage.setImageResource(R.drawable.superphy_icon_200x200);
        } else if(selectedCardType.equals("Super INT")) {
            typeImage.setImageResource(R.drawable.sint_icon_200x200);
        } else if(selectedCardType.equals("Super AGL")) {
            typeImage.setImageResource(R.drawable.sagl_icon_200x200);
        } else if(selectedCardType.equals("Super STR")) {
            typeImage.setImageResource(R.drawable.sstr_icon_200x200);
        } else if(selectedCardType.equals("Super TEQ")) {
            typeImage.setImageResource(R.drawable.steq_icon_200x200);
        } else if(selectedCardType.equals("Extreme PHY")) {
            typeImage.setImageResource(R.drawable.ephy_icon_200x200);
        } else if(selectedCardType.equals("Extreme INT")) {
            typeImage.setImageResource(R.drawable.eint_icon_200x200);
        } else if(selectedCardType.equals("Extreme AGL")) {
            typeImage.setImageResource(R.drawable.eagl_icon_200x200);
        } else if(selectedCardType.equals("Extreme STR")) {
            typeImage.setImageResource(R.drawable.estr_icon_200x200);
        } else if(selectedCardType.equals("Extreme TEQ")) {
            typeImage.setImageResource(R.drawable.eteq_icon_200x200);
        } else {
            Log.d(TAG,"Error determining card type");
        }
    }

    /**
     * Initialize the NewCardViewActivity's views with the data from the CardInfoDatabase.java class
     * @param selectedItemPosition Used to initialize this activity's views if the intent was called from the MainScreen Fragment
     */
    private void initCardViewMainScreen(int selectedItemPosition,int filterOptionSelected) {
        if(filterOptionSelected == 0) {
            backgroundImage.setBackgroundResource((CardInfoDatabase.cardDatabase[selectedItemPosition].getCardArt()));
            leader_skill_text = CardInfoDatabase.cardDatabase[selectedItemPosition].getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = CardInfoDatabase.cardDatabase[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.cardDatabase[selectedItemPosition].getSuperAttackDesc();
            passive_skill_text = CardInfoDatabase.cardDatabase[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.cardDatabase[selectedItemPosition].getPassiveSkillDesc();
            hp.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getHp());
            att.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getAtt());
            def.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getDef());
            cost.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getCost());
            linkSkill1.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getLinkSkills().get(0));
            linkSkill2.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getLinkSkills().get(1));
            linkSkill3.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getLinkSkills().get(2));
            linkSkill4.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getLinkSkills().get(3));
            linkSkill5.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getLinkSkills().get(4));
            linkSkill6.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getLinkSkills().get(5));
            linkSkill7.setText(CardInfoDatabase.cardDatabase[selectedItemPosition].getLinkSkills().get(6));
            Log.i(TAG,"leader skill: " + leader_skill_text);
            // Dialog text variables
            leaderDialogText = CardInfoDatabase.cardDatabase[selectedItemPosition].getLeaderSkill();
            superDialogText = CardInfoDatabase.cardDatabase[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.cardDatabase[selectedItemPosition].getSuperAttackDesc();
            passiveDialogText = CardInfoDatabase.cardDatabase[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.cardDatabase[selectedItemPosition].getPassiveSkillDesc();

            determineRarity(CardInfoDatabase.cardDatabase[selectedItemPosition].getRarity());
            determineType(CardInfoDatabase.cardDatabase[selectedItemPosition].getType());

            //TODO:sp find a better solution for this e.g: turn it into a method
            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(CardInfoDatabase.cardDatabase[selectedItemPosition].getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelected == 1) {
            backgroundImage.setBackgroundResource(CardInfoDatabase.LRCards[selectedItemPosition].getCardArt());
            leader_skill_text = CardInfoDatabase.LRCards[selectedItemPosition].getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = CardInfoDatabase.LRCards[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.LRCards[selectedItemPosition].getSuperAttackDesc();
            passive_skill_text = CardInfoDatabase.LRCards[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.LRCards[selectedItemPosition].getPassiveSkillDesc();
            hp.setText(CardInfoDatabase.LRCards[selectedItemPosition].getHp());
            att.setText(CardInfoDatabase.LRCards[selectedItemPosition].getAtt());
            def.setText(CardInfoDatabase.LRCards[selectedItemPosition].getDef());
            cost.setText(CardInfoDatabase.LRCards[selectedItemPosition].getCost());
            linkSkill1.setText(CardInfoDatabase.LRCards[selectedItemPosition].getLinkSkills().get(0));
            linkSkill2.setText(CardInfoDatabase.LRCards[selectedItemPosition].getLinkSkills().get(1));
            linkSkill3.setText(CardInfoDatabase.LRCards[selectedItemPosition].getLinkSkills().get(2));
            linkSkill4.setText(CardInfoDatabase.LRCards[selectedItemPosition].getLinkSkills().get(3));
            linkSkill5.setText(CardInfoDatabase.LRCards[selectedItemPosition].getLinkSkills().get(4));
            linkSkill6.setText(CardInfoDatabase.LRCards[selectedItemPosition].getLinkSkills().get(5));
            linkSkill7.setText(CardInfoDatabase.LRCards[selectedItemPosition].getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = CardInfoDatabase.LRCards[selectedItemPosition].getLeaderSkill();
            superDialogText = CardInfoDatabase.LRCards[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.LRCards[selectedItemPosition].getSuperAttackDesc();
            passiveDialogText = CardInfoDatabase.LRCards[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.LRCards[selectedItemPosition].getPassiveSkillDesc();

            rarityImage.setImageResource(R.drawable.lr_rarity_icon);
            determineType(CardInfoDatabase.LRCards[selectedItemPosition].getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(CardInfoDatabase.LRCards[selectedItemPosition].getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelected == 2) {
            backgroundImage.setBackgroundResource(CardInfoDatabase.URCards[selectedItemPosition].getCardArt());
            leader_skill_text = CardInfoDatabase.URCards[selectedItemPosition].getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = CardInfoDatabase.URCards[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.URCards[selectedItemPosition].getSuperAttackDesc();
            passive_skill_text = CardInfoDatabase.URCards[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.URCards[selectedItemPosition].getPassiveSkillDesc();
            hp.setText(CardInfoDatabase.URCards[selectedItemPosition].getHp());
            att.setText(CardInfoDatabase.URCards[selectedItemPosition].getAtt());
            def.setText(CardInfoDatabase.URCards[selectedItemPosition].getDef());
            cost.setText(CardInfoDatabase.URCards[selectedItemPosition].getCost());
            linkSkill1.setText(CardInfoDatabase.URCards[selectedItemPosition].getLinkSkills().get(0));
            linkSkill2.setText(CardInfoDatabase.URCards[selectedItemPosition].getLinkSkills().get(1));
            linkSkill3.setText(CardInfoDatabase.URCards[selectedItemPosition].getLinkSkills().get(2));
            linkSkill4.setText(CardInfoDatabase.URCards[selectedItemPosition].getLinkSkills().get(3));
            linkSkill5.setText(CardInfoDatabase.URCards[selectedItemPosition].getLinkSkills().get(4));
            linkSkill6.setText(CardInfoDatabase.URCards[selectedItemPosition].getLinkSkills().get(5));
            linkSkill7.setText(CardInfoDatabase.URCards[selectedItemPosition].getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = CardInfoDatabase.URCards[selectedItemPosition].getLeaderSkill();
            superDialogText = CardInfoDatabase.URCards[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.URCards[selectedItemPosition].getSuperAttackDesc();
            passiveDialogText = CardInfoDatabase.URCards[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.URCards[selectedItemPosition].getPassiveSkillDesc();

            rarityImage.setImageResource(R.drawable.ur_icon);
            determineType(CardInfoDatabase.URCards[selectedItemPosition].getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(CardInfoDatabase.URCards[selectedItemPosition].getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelected == 3) {
            backgroundImage.setBackgroundResource(CardInfoDatabase.SSRCards[selectedItemPosition].getCardArt());
            leader_skill_text = CardInfoDatabase.SSRCards[selectedItemPosition].getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = CardInfoDatabase.SSRCards[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.SSRCards[selectedItemPosition].getSuperAttackDesc();
            passive_skill_text = CardInfoDatabase.SSRCards[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.SSRCards[selectedItemPosition].getPassiveSkillDesc();
            hp.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getHp());
            att.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getAtt());
            def.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getDef());
            cost.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getCost());
            linkSkill1.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getLinkSkills().get(0));
            linkSkill2.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getLinkSkills().get(1));
            linkSkill3.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getLinkSkills().get(2));
            linkSkill4.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getLinkSkills().get(3));
            linkSkill5.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getLinkSkills().get(4));
            linkSkill6.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getLinkSkills().get(5));
            linkSkill7.setText(CardInfoDatabase.SSRCards[selectedItemPosition].getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = CardInfoDatabase.SSRCards[selectedItemPosition].getLeaderSkill();
            superDialogText = CardInfoDatabase.SSRCards[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.SSRCards[selectedItemPosition].getSuperAttackDesc();
            passiveDialogText = CardInfoDatabase.SSRCards[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.SSRCards[selectedItemPosition].getPassiveSkillDesc();

            rarityImage.setImageResource(R.drawable.ssr_icon);
            determineType(CardInfoDatabase.SSRCards[selectedItemPosition].getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(CardInfoDatabase.SSRCards[selectedItemPosition].getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelected == 4) {
            backgroundImage.setBackgroundResource(CardInfoDatabase.Leaders120[selectedItemPosition].getCardArt());
            leader_skill_text = CardInfoDatabase.Leaders120[selectedItemPosition].getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = CardInfoDatabase.Leaders120[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.Leaders120[selectedItemPosition].getSuperAttackDesc();
            passive_skill_text = CardInfoDatabase.Leaders120[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.Leaders120[selectedItemPosition].getPassiveSkillDesc();
            hp.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getHp());
            att.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getAtt());
            def.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getDef());
            cost.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getCost());
            linkSkill1.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getLinkSkills().get(0));
            linkSkill2.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getLinkSkills().get(1));
            linkSkill3.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getLinkSkills().get(2));
            linkSkill4.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getLinkSkills().get(3));
            linkSkill5.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getLinkSkills().get(4));
            linkSkill6.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getLinkSkills().get(5));
            linkSkill7.setText(CardInfoDatabase.Leaders120[selectedItemPosition].getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = CardInfoDatabase.Leaders120[selectedItemPosition].getLeaderSkill();
            superDialogText = CardInfoDatabase.Leaders120[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.Leaders120[selectedItemPosition].getSuperAttackDesc();
            passiveDialogText = CardInfoDatabase.Leaders120[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.Leaders120[selectedItemPosition].getPassiveSkillDesc();

            determineRarity(CardInfoDatabase.Leaders120[selectedItemPosition].getRarity());
            determineType(CardInfoDatabase.Leaders120[selectedItemPosition].getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(CardInfoDatabase.Leaders120[selectedItemPosition].getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelected == 5) {
            backgroundImage.setBackgroundResource(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getCardArt());
            leader_skill_text = CardInfoDatabase.CategoryLeaders[selectedItemPosition].getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = CardInfoDatabase.CategoryLeaders[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.CategoryLeaders[selectedItemPosition].getSuperAttackDesc();
            passive_skill_text = CardInfoDatabase.CategoryLeaders[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.CategoryLeaders[selectedItemPosition].getPassiveSkillDesc();
            hp.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getHp());
            att.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getAtt());
            def.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getDef());
            cost.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getCost());
            linkSkill1.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getLinkSkills().get(0));
            linkSkill2.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getLinkSkills().get(1));
            linkSkill3.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getLinkSkills().get(2));
            linkSkill4.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getLinkSkills().get(3));
            linkSkill5.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getLinkSkills().get(4));
            linkSkill6.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getLinkSkills().get(5));
            linkSkill7.setText(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = CardInfoDatabase.CategoryLeaders[selectedItemPosition].getLeaderSkill();
            superDialogText = CardInfoDatabase.CategoryLeaders[selectedItemPosition].getSuperAttackName() + "-" + CardInfoDatabase.CategoryLeaders[selectedItemPosition].getSuperAttackDesc();
            passiveDialogText = CardInfoDatabase.CategoryLeaders[selectedItemPosition].getPassiveSkillName() + "-" + CardInfoDatabase.CategoryLeaders[selectedItemPosition].getPassiveSkillDesc();

            determineRarity(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getRarity());
            determineType(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(CardInfoDatabase.CategoryLeaders[selectedItemPosition].getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else {
            Log.d("TAG","Error loading the card info in the card view activity");
        }
    }

    /**
     * Initialize the NewCardViewActivity's views with the data from the GlobalDataHolder.java class
     * @param selectedItemPosition Used to initialize this activity's views if the intent was called from the Global Fragment
     */
    private void initCardViewGLB(int selectedItemPosition,int filterOptionSelectedGLB) {
        if(filterOptionSelectedGLB == 0) {
            backgroundImage.setBackgroundResource(GlobalDataHolder.cards.get(selectedItemPosition).getCardArt());
            leader_skill_text = GlobalDataHolder.cards.get(selectedItemPosition).getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = GlobalDataHolder.cards.get(selectedItemPosition).getSuperAttackName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passive_skill_text = GlobalDataHolder.cards.get(selectedItemPosition).getPassiveSkillName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();
            hp.setText(GlobalDataHolder.cards.get(selectedItemPosition).getHp());
            att.setText(GlobalDataHolder.cards.get(selectedItemPosition).getAtt());
            def.setText(GlobalDataHolder.cards.get(selectedItemPosition).getDef());
            cost.setText(GlobalDataHolder.cards.get(selectedItemPosition).getCost());
            linkSkill1.setText(GlobalDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(0));
            linkSkill2.setText(GlobalDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(1));
            linkSkill3.setText(GlobalDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(2));
            linkSkill4.setText(GlobalDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(3));
            linkSkill5.setText(GlobalDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(4));
            linkSkill6.setText(GlobalDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(5));
            linkSkill7.setText(GlobalDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = GlobalDataHolder.cards.get(selectedItemPosition).getLeaderSkill();
            superDialogText = GlobalDataHolder.cards.get(selectedItemPosition).getSuperAttackName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passiveDialogText = GlobalDataHolder.cards.get(selectedItemPosition).getPassiveSkillName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();

            determineRarity(GlobalDataHolder.cards.get(selectedItemPosition).getRarity());
            determineType(GlobalDataHolder.cards.get(selectedItemPosition).getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(GlobalDataHolder.cards.get(selectedItemPosition).getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelectedGLB == 1) {
            backgroundImage.setBackgroundResource(GlobalDataHolder.LRCards.get(selectedItemPosition).getCardArt());
            leader_skill_text = GlobalDataHolder.LRCards.get(selectedItemPosition).getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = GlobalDataHolder.LRCards.get(selectedItemPosition).getSuperAttackName() + "-" + GlobalDataHolder.LRCards.get(selectedItemPosition).getSuperAttackDesc();
            passive_skill_text = GlobalDataHolder.LRCards.get(selectedItemPosition).getPassiveSkillName() + "-" + GlobalDataHolder.LRCards.get(selectedItemPosition).getPassiveSkillDesc();
            hp.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getHp());
            att.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getAtt());
            def.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getDef());
            cost.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getCost());
            linkSkill1.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(0));
            linkSkill2.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(1));
            linkSkill3.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(2));
            linkSkill4.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(3));
            linkSkill5.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(4));
            linkSkill6.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(5));
            linkSkill7.setText(GlobalDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = GlobalDataHolder.LRCards.get(selectedItemPosition).getLeaderSkill();
            superDialogText = GlobalDataHolder.LRCards.get(selectedItemPosition).getSuperAttackName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passiveDialogText = GlobalDataHolder.LRCards.get(selectedItemPosition).getPassiveSkillName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();

            rarityImage.setImageResource(R.drawable.lr_rarity_icon);
            determineType(GlobalDataHolder.LRCards.get(selectedItemPosition).getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(GlobalDataHolder.LRCards.get(selectedItemPosition).getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelectedGLB == 2) {
            backgroundImage.setBackgroundResource(GlobalDataHolder.URCards.get(selectedItemPosition).getCardArt());
            leader_skill_text = GlobalDataHolder.URCards.get(selectedItemPosition).getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = GlobalDataHolder.URCards.get(selectedItemPosition).getSuperAttackName() + "-" + GlobalDataHolder.URCards.get(selectedItemPosition).getSuperAttackDesc();
            passive_skill_text = GlobalDataHolder.URCards.get(selectedItemPosition).getPassiveSkillName() + "-" + GlobalDataHolder.URCards.get(selectedItemPosition).getPassiveSkillDesc();
            hp.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getHp());
            att.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getAtt());
            def.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getDef());
            cost.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getCost());
            linkSkill1.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(0));
            linkSkill2.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(1));
            linkSkill3.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(2));
            linkSkill4.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(3));
            linkSkill5.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(4));
            linkSkill6.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(5));
            linkSkill7.setText(GlobalDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = GlobalDataHolder.URCards.get(selectedItemPosition).getLeaderSkill();
            superDialogText = GlobalDataHolder.URCards.get(selectedItemPosition).getSuperAttackName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passiveDialogText = GlobalDataHolder.URCards.get(selectedItemPosition).getPassiveSkillName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();

            rarityImage.setImageResource(R.drawable.ur_icon);
            determineType(GlobalDataHolder.URCards.get(selectedItemPosition).getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(GlobalDataHolder.URCards.get(selectedItemPosition).getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelectedGLB == 3) {
            backgroundImage.setBackgroundResource(GlobalDataHolder.SSRCards.get(selectedItemPosition).getCardArt());
            leader_skill_text = GlobalDataHolder.SSRCards.get(selectedItemPosition).getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = GlobalDataHolder.SSRCards.get(selectedItemPosition).getSuperAttackName() + "-" + GlobalDataHolder.SSRCards.get(selectedItemPosition).getSuperAttackDesc();
            passive_skill_text = GlobalDataHolder.SSRCards.get(selectedItemPosition).getPassiveSkillName() + "-" + GlobalDataHolder.SSRCards.get(selectedItemPosition).getPassiveSkillDesc();
            hp.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getHp());
            att.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getAtt());
            def.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getDef());
            cost.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getCost());
            linkSkill1.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(0));
            linkSkill2.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(1));
            linkSkill3.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(2));
            linkSkill4.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(3));
            linkSkill5.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(4));
            linkSkill6.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(5));
            linkSkill7.setText(GlobalDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = GlobalDataHolder.SSRCards.get(selectedItemPosition).getLeaderSkill();
            superDialogText = GlobalDataHolder.SSRCards.get(selectedItemPosition).getSuperAttackName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passiveDialogText = GlobalDataHolder.SSRCards.get(selectedItemPosition).getPassiveSkillName() + "-" + GlobalDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();

            rarityImage.setImageResource(R.drawable.ssr_icon);
            determineType(GlobalDataHolder.SSRCards.get(selectedItemPosition).getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(GlobalDataHolder.SSRCards.get(selectedItemPosition).getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else {
            Log.d("TAG","Error loading the card info in the card view activity from GLOBAL!");
        }
    }

    /**
     * Initialize the NewCardViewActivity's views with the data from the JPDataHolder.java class
     * @param selectedItemPosition Used to initialize this activity's views if the intent was called from the JP Fragment
     */
    private void initCardViewJP(int selectedItemPosition,int filterOptionSelectedJP) {
        if(filterOptionSelectedJP == 0) {
            backgroundImage.setBackgroundResource(JPDataHolder.cards.get(selectedItemPosition).getCardArt());
            leader_skill_text = JPDataHolder.cards.get(selectedItemPosition).getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = JPDataHolder.cards.get(selectedItemPosition).getSuperAttackName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passive_skill_text = JPDataHolder.cards.get(selectedItemPosition).getPassiveSkillName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();
            hp.setText(JPDataHolder.cards.get(selectedItemPosition).getHp());
            att.setText(JPDataHolder.cards.get(selectedItemPosition).getAtt());
            def.setText(JPDataHolder.cards.get(selectedItemPosition).getDef());
            cost.setText(JPDataHolder.cards.get(selectedItemPosition).getCost());
            linkSkill1.setText(JPDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(0));
            linkSkill2.setText(JPDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(1));
            linkSkill3.setText(JPDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(2));
            linkSkill4.setText(JPDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(3));
            linkSkill5.setText(JPDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(4));
            linkSkill6.setText(JPDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(5));
            linkSkill7.setText(JPDataHolder.cards.get(selectedItemPosition).getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = JPDataHolder.cards.get(selectedItemPosition).getLeaderSkill();
            superDialogText = JPDataHolder.cards.get(selectedItemPosition).getSuperAttackName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passiveDialogText = JPDataHolder.cards.get(selectedItemPosition).getPassiveSkillName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();

            determineRarity(JPDataHolder.cards.get(selectedItemPosition).getRarity());
            determineType(JPDataHolder.cards.get(selectedItemPosition).getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(JPDataHolder.cards.get(selectedItemPosition).getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelectedJP == 1) {
            backgroundImage.setBackgroundResource(JPDataHolder.LRCards.get(selectedItemPosition).getCardArt());
            leader_skill_text = JPDataHolder.LRCards.get(selectedItemPosition).getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = JPDataHolder.LRCards.get(selectedItemPosition).getSuperAttackName() + "-" + JPDataHolder.LRCards.get(selectedItemPosition).getSuperAttackDesc();
            passive_skill_text = JPDataHolder.LRCards.get(selectedItemPosition).getPassiveSkillName() + "-" + JPDataHolder.LRCards.get(selectedItemPosition).getPassiveSkillDesc();
            hp.setText(JPDataHolder.LRCards.get(selectedItemPosition).getHp());
            att.setText(JPDataHolder.LRCards.get(selectedItemPosition).getAtt());
            def.setText(JPDataHolder.LRCards.get(selectedItemPosition).getDef());
            cost.setText(JPDataHolder.LRCards.get(selectedItemPosition).getCost());
            linkSkill1.setText(JPDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(0));
            linkSkill2.setText(JPDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(1));
            linkSkill3.setText(JPDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(2));
            linkSkill4.setText(JPDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(3));
            linkSkill5.setText(JPDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(4));
            linkSkill6.setText(JPDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(5));
            linkSkill7.setText(JPDataHolder.LRCards.get(selectedItemPosition).getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = JPDataHolder.LRCards.get(selectedItemPosition).getLeaderSkill();
            superDialogText = JPDataHolder.LRCards.get(selectedItemPosition).getSuperAttackName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passiveDialogText = JPDataHolder.LRCards.get(selectedItemPosition).getPassiveSkillName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();

            rarityImage.setImageResource(R.drawable.lr_rarity_icon);
            determineType(JPDataHolder.LRCards.get(selectedItemPosition).getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(JPDataHolder.LRCards.get(selectedItemPosition).getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelectedJP == 2) {
            backgroundImage.setBackgroundResource(JPDataHolder.URCards.get(selectedItemPosition).getCardArt());
            leader_skill_text = JPDataHolder.URCards.get(selectedItemPosition).getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = JPDataHolder.URCards.get(selectedItemPosition).getSuperAttackName() + "-" + JPDataHolder.URCards.get(selectedItemPosition).getSuperAttackDesc();
            passive_skill_text = JPDataHolder.URCards.get(selectedItemPosition).getPassiveSkillName() + "-" + JPDataHolder.URCards.get(selectedItemPosition).getPassiveSkillDesc();
            hp.setText(JPDataHolder.URCards.get(selectedItemPosition).getHp());
            att.setText(JPDataHolder.URCards.get(selectedItemPosition).getAtt());
            def.setText(JPDataHolder.URCards.get(selectedItemPosition).getDef());
            cost.setText(JPDataHolder.URCards.get(selectedItemPosition).getCost());
            linkSkill1.setText(JPDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(0));
            linkSkill2.setText(JPDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(1));
            linkSkill3.setText(JPDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(2));
            linkSkill4.setText(JPDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(3));
            linkSkill5.setText(JPDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(4));
            linkSkill6.setText(JPDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(5));
            linkSkill7.setText(JPDataHolder.URCards.get(selectedItemPosition).getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = JPDataHolder.URCards.get(selectedItemPosition).getLeaderSkill();
            superDialogText = JPDataHolder.URCards.get(selectedItemPosition).getSuperAttackName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passiveDialogText = JPDataHolder.URCards.get(selectedItemPosition).getPassiveSkillName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();

            rarityImage.setImageResource(R.drawable.ur_icon);
            determineType(JPDataHolder.URCards.get(selectedItemPosition).getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(JPDataHolder.URCards.get(selectedItemPosition).getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else if(filterOptionSelectedJP == 3) {
            backgroundImage.setBackgroundResource(JPDataHolder.SSRCards.get(selectedItemPosition).getCardArt());
            leader_skill_text = JPDataHolder.SSRCards.get(selectedItemPosition).getLeaderSkill();
            leaderSkill.setText(leader_skill_text);
            super_attack_text = JPDataHolder.SSRCards.get(selectedItemPosition).getSuperAttackName() + "-" + JPDataHolder.SSRCards.get(selectedItemPosition).getSuperAttackDesc();
            passive_skill_text = JPDataHolder.SSRCards.get(selectedItemPosition).getPassiveSkillName() + "-" + JPDataHolder.SSRCards.get(selectedItemPosition).getPassiveSkillDesc();
            hp.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getHp());
            att.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getAtt());
            def.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getDef());
            cost.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getCost());
            linkSkill1.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(0));
            linkSkill2.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(1));
            linkSkill3.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(2));
            linkSkill4.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(3));
            linkSkill5.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(4));
            linkSkill6.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(5));
            linkSkill7.setText(JPDataHolder.SSRCards.get(selectedItemPosition).getLinkSkills().get(6));
            // Dialog text variables
            leaderDialogText = JPDataHolder.SSRCards.get(selectedItemPosition).getLeaderSkill();
            superDialogText = JPDataHolder.SSRCards.get(selectedItemPosition).getSuperAttackName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getSuperAttackDesc();
            passiveDialogText = JPDataHolder.SSRCards.get(selectedItemPosition).getPassiveSkillName() + "-" + JPDataHolder.cards.get(selectedItemPosition).getPassiveSkillDesc();

            rarityImage.setImageResource(R.drawable.ssr_icon);
            determineType(JPDataHolder.SSRCards.get(selectedItemPosition).getType());

            nameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardDetailsDialog dialog = new CardDetailsDialog();
                    dialog.setTextToDisplay(JPDataHolder.SSRCards.get(selectedItemPosition).getName());
                    dialog.show(getFragmentManager(),"NAME_DIALOG");
                }
            });

        } else{
            Log.d("TAG","Error loading the card info in the card view activity from JP!");
        }
    }
}
