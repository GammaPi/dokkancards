package com.dcv.spdesigns.dokkancards.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.dcv.spdesigns.dokkancards.R;

/**
 * This dialog is created when the user clicks on the
 * "Details" text in any CardView Activity instance and shows
 * the desired text in full length.
 */
public class CardDetailsDialog extends DialogFragment {

    private String textToDisplay = "";

    public void setTextToDisplay(String text) {
        textToDisplay = text;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme)
                .setMessage(textToDisplay)
                .setPositiveButton("OK",null);
        return builder.create();
    }
}
