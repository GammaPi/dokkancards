package com.dcv.spdesigns.dokkancards.ui;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.main.LrRecyclerViewAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.main.SsrRecyclerViewAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.main.UrRecyclerViewAdapter;
import com.dcv.spdesigns.dokkancards.model.glb.GlobalDataHolder;
import com.dcv.spdesigns.dokkancards.model.jp.JPDataHolder;
import com.dcv.spdesigns.dokkancards.model.main.CardInfoDatabase;
import com.dcv.spdesigns.dokkancards.model.main.MainRecyclerViewImageAdapter;
import com.dcv.spdesigns.dokkancards.model.main.ScrollSpeedControllerLayoutManager;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainScreenFragmentV2 extends Fragment {

    public static RecyclerView recyclerView;

    public MainScreenFragmentV2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_screen_fragment_v2,container,false);
        recyclerView = view.findViewById(R.id.recycleView);
        RecyclerView.LayoutManager recyclerViewLayoutManager = new GridLayoutManager(getActivity(), 4);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        RecyclerView.Adapter recyclerView_Adapter = new MainRecyclerViewImageAdapter(getActivity());
        recyclerView.setAdapter(recyclerView_Adapter);

        // recycler view scrolling optimization
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(5);

        registerForContextMenu(recyclerView);
        return view;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);

        int position = item.getGroupId(); // clicked view's position

        if(item.getTitle().equals("Add Card to GLB")) {
            Toasty.success(this.getActivity(),"Card added to GLB!",Toast.LENGTH_SHORT,true).show();
            addSelectedCardToGlobalUserBox(position);
        } else if (item.getTitle().equals("Add Card to JP")) {
            Toasty.success(this.getActivity(),"Card added to JP!",Toast.LENGTH_SHORT,true).show();
            addSelectedCardToJPUserBox(position);
        } else {
            Toasty.error(this.getActivity(),"Looks like yamcha messed something up! (Error code: 01CIS)",Toast.LENGTH_LONG,true).show();
            return false;
        }
        return false;
    }

    /**
     * Add the selected card's icon to the Global User Box
     * @param position Holds the selected card's position.
     * This is used to define where the icon should be placed in the grid &
     * which icon from the DataBase was selected
     */
    private void addSelectedCardToGlobalUserBox(int position) {
        //Log.i("Position:", position +"");
        GlobalDataHolder.cards.add(CardInfoDatabase.cardDatabase[position]);
    }

    /**
     * Add the selected card's icon to the JP User Box
     * @param position Holds the selected card's position.
     * This is used to define where the icon should be placed in the grid &
     * which icon from the DataBase was selected
     */
    private void addSelectedCardToJPUserBox(int position) {
        JPDataHolder.cards.add(CardInfoDatabase.cardDatabase[position]);
    }

    /**
     * This method sets the main Grid's image adapter based on the selected
     * filter dialog option. Check the SortingDialog.java class for more info.
     * @param adapter The adapter which will be set as the main adapter for the grid.
     */
    public static void setMainGridImageAdapter(RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    /**
     * Checks if any filter adapter other than the Main one is currently
     * loaded to the fragment's gridView and if it is, it disables the
     * card addition feature so that there are no bugs!
     * @return true if the above condition is true or false if it isn't
     */
    public static boolean canAddCardToBox() {
        RecyclerView.Adapter currentAdapter = recyclerView.getAdapter();
        return !(currentAdapter instanceof LrRecyclerViewAdapter || currentAdapter instanceof UrRecyclerViewAdapter || currentAdapter instanceof SsrRecyclerViewAdapter);
    }

}
