package com.dcv.spdesigns.dokkancards.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

public class PermissionExplanationDialog extends DialogFragment {
    private int permID;

    public void setPermissionId(int id) {
        permID = id;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(determineMessage(permID))
                .setTitle("Permission Explanation")
                .setPositiveButton("OK",null);
        return builder.create();
    }

    private String determineMessage(int permID) {
        String msg = "";
        switch (permID) {
            case 0: //Internet perm
                msg = "The Internet permission is required so that the app can download the card images from the web.";
                break;
            case 1:
                msg = "The Wake-Lock permission is required so that while the app is downloading the required images,the process doesn't stop unexpectedly.";
                break;
            case 2:
                msg = "The permission to access your network's state is required so that the app can connect to the internet for the download process to begin.";
                break;
            case 3:
                msg = "The wifi access permission is required so that the app can use the wifi connection to download the required files.";
                break;
            case 4:
                msg = "The app needs your permission to write to your external storage so that it can save the downloaded files on your device.";
                break;
            case 5:
                msg = "The app needs your permission to read your external storage in order to read the downloaded files.";
                break;
        }
        return msg;
    }
}
