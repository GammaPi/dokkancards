package com.dcv.spdesigns.dokkancards.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.glb.GlobalLrImageAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.glb.GlobalSsrImageAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.glb.GlobalUrImageAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.jp.JpLrImageAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.jp.JpSsrImageAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.jp.JpUrImageAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.main.CategoryLeadersRecyclerviewAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.main.Leaders120RecyclerViewAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.main.LrRecyclerViewAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.main.SsrRecyclerViewAdapter;
import com.dcv.spdesigns.dokkancards.model.filter_dialog.main.UrRecyclerViewAdapter;
import com.dcv.spdesigns.dokkancards.model.glb.UserBoxGlbImageAdapter;
import com.dcv.spdesigns.dokkancards.model.jp.UserBoxJpImageAdapter;
import com.dcv.spdesigns.dokkancards.model.main.MainRecyclerViewImageAdapter;

import es.dmoral.toasty.Toasty;

/**
 * This class instantiates a dialog which opens up
 * when the user clicks on the sorting icon in the toolbar
 * and provides a plethora of sorting/filter options for the current
 * fragment
 */
public class SortingDialog extends DialogFragment {

    private static String TAG = SortingDialog.class.getSimpleName();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Create a builder to make the dialog building process easier
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Filter Options");
        builder.setSingleChoiceItems(R.array.sorting_options, 0, //TODO:sp instead of having the checked item as static at 0, change it dynamically
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int itemPos) {

                        // This line is used to get the current fragment from the FrameLayout Container
                        android.support.v4.app.Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.FrameLayoutContainer);
                        SetDialogOption(itemPos, currentFragment);
                    }
                });
        builder.setPositiveButton("OK",null);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });
        return builder.create();
    }

    /**
     * This method determines which sorting/filter option was selected
     * from the filter dialog and uses that option to dynamically change the
     * current fragment's image adapter so that it displays the desired Card
     * objects only.
     * @param itemPos The option that was selected in the filter dialog
     * @param currentFragment The current fragment whose image adapter will be(possibly) changed.
     */
    private void SetDialogOption(int itemPos, Fragment currentFragment) {
        if(itemPos == 0 && (currentFragment instanceof MainScreenFragmentV2)) {
            Toasty.info(this.getActivity(),"Database sorted successfully!",Toast.LENGTH_LONG,true).show();
            MainScreenFragmentV2.setMainGridImageAdapter(new MainRecyclerViewImageAdapter(getActivity()));
            MainRecyclerViewImageAdapter.setFilterDialogOptionSelected(itemPos);
        } else if(itemPos == 0 && (currentFragment instanceof UserBoxGLBFragment)) {
            Toasty.info(this.getActivity(),"Database sorted successfully!",Toast.LENGTH_LONG,true).show();
            UserBoxGLBFragment.setGLBFragmentAdapter(new UserBoxGlbImageAdapter(getActivity()));
            UserBoxGLBFragment.setFilterOptionSelected(itemPos);
        } else if(itemPos == 0 && (currentFragment instanceof UserBoxJPFragment)) {
            Toasty.info(this.getActivity(),"Database sorted successfully!",Toast.LENGTH_LONG,true).show();
            UserBoxJPFragment.setJPFragmentAdapter(new UserBoxJpImageAdapter(getContext()));
            UserBoxJPFragment.setFilterOptionSelectedJP(itemPos);
        } else if(itemPos == 1 && (currentFragment instanceof MainScreenFragmentV2)) { // If the LR Cards item has been clicked
            Toasty.info(this.getActivity(),"Showing only LR Cards",Toast.LENGTH_LONG,true).show();
            MainScreenFragmentV2.setMainGridImageAdapter(new LrRecyclerViewAdapter(getActivity()));
            LrRecyclerViewAdapter.setFilterDialogOptionSelected(itemPos);
        } else if(itemPos == 1 && (currentFragment instanceof UserBoxGLBFragment)) {
            Toasty.info(this.getActivity(),"Showing only LR Cards",Toast.LENGTH_LONG,true).show();
            UserBoxGLBFragment.setGLBFragmentAdapter(new GlobalLrImageAdapter(getActivity()));
            UserBoxGLBFragment.setFilterOptionSelected(itemPos);
        } else if(itemPos == 1 && (currentFragment instanceof UserBoxJPFragment)) {
            Toasty.info(this.getActivity(),"Showing only LR Cards",Toast.LENGTH_LONG,true).show();
            UserBoxJPFragment.setJPFragmentAdapter(new JpLrImageAdapter(getContext()));
            UserBoxJPFragment.setFilterOptionSelectedJP(itemPos);
        } else if (itemPos == 2 && (currentFragment instanceof MainScreenFragmentV2)) {
            Toasty.info(this.getActivity(),"Showing only UR Cards",Toast.LENGTH_LONG,true).show();
            MainScreenFragmentV2.setMainGridImageAdapter(new UrRecyclerViewAdapter(getActivity()));
            UrRecyclerViewAdapter.setFilterDialogOptionSelected(itemPos);
        } else if(itemPos == 2 && (currentFragment instanceof UserBoxGLBFragment)) {
            Toasty.info(this.getActivity(),"Showing only UR Cards",Toast.LENGTH_LONG,true).show();
            UserBoxGLBFragment.setGLBFragmentAdapter(new GlobalUrImageAdapter(getContext()));
            UserBoxGLBFragment.setFilterOptionSelected(itemPos);
        } else if(itemPos == 2 && (currentFragment instanceof UserBoxJPFragment)) {
            Toasty.info(this.getActivity(),"Showing only UR Cards",Toast.LENGTH_LONG,true).show();
            UserBoxJPFragment.setJPFragmentAdapter(new JpUrImageAdapter(getContext()));
            UserBoxJPFragment.setFilterOptionSelectedJP(itemPos);
        } else if(itemPos == 3 && (currentFragment instanceof MainScreenFragmentV2)) {
            Toasty.info(this.getActivity(),"Showing only SSR Cards",Toast.LENGTH_LONG,true).show();
            MainScreenFragmentV2.setMainGridImageAdapter(new SsrRecyclerViewAdapter(getActivity()));
            SsrRecyclerViewAdapter.setFilterDialogOptionSelected(itemPos);
        }  else if(itemPos == 3 && (currentFragment instanceof UserBoxGLBFragment)) {
            Toasty.info(this.getActivity(),"Showing only SSR Cards",Toast.LENGTH_LONG,true).show();
            UserBoxGLBFragment.setGLBFragmentAdapter(new GlobalSsrImageAdapter(getActivity()));
            UserBoxGLBFragment.setFilterOptionSelected(itemPos);
        } else if(itemPos == 3 && (currentFragment instanceof UserBoxJPFragment)) {
            Toasty.info(this.getActivity(),"Showing only SSR Cards",Toast.LENGTH_LONG,true).show();
            UserBoxJPFragment.setJPFragmentAdapter(new JpSsrImageAdapter(getActivity()));
            UserBoxJPFragment.setFilterOptionSelectedJP(itemPos);
        } else if(itemPos == 4 && (currentFragment instanceof MainScreenFragmentV2)) {
            Toasty.info(this.getActivity(),"Showing only the 120% Leader Cards",Toast.LENGTH_LONG,true).show();
            MainScreenFragmentV2.setMainGridImageAdapter(new Leaders120RecyclerViewAdapter(getActivity()));
            Leaders120RecyclerViewAdapter.setFilterDialogOptionSelected(itemPos);
        } else if(itemPos == 4 && ((currentFragment instanceof UserBoxGLBFragment) || (currentFragment instanceof UserBoxJPFragment))) {
            Toasty.error(this.getActivity(),"This filter isn't available for userboxes!",Toast.LENGTH_LONG,true).show();
        } else if(itemPos == 5 && (currentFragment instanceof MainScreenFragmentV2)) {
            Toasty.info(this.getActivity(),"Showing only the Category Leader Cards",Toast.LENGTH_LONG,true).show();
            MainScreenFragmentV2.setMainGridImageAdapter(new CategoryLeadersRecyclerviewAdapter(getActivity()));
            CategoryLeadersRecyclerviewAdapter.setFilterDialogOptionSelected(itemPos);
        } else if(itemPos == 5 && ((currentFragment instanceof UserBoxGLBFragment) || (currentFragment instanceof UserBoxJPFragment))) {
            Toasty.error(this.getActivity(),"This filter isn't available for userboxes!",Toast.LENGTH_LONG,true).show();
        }  else {
            Log.v(TAG,"Problem in the sorting dialog class!");
        }
    }
}
