package com.dcv.spdesigns.dokkancards.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dcv.spdesigns.dokkancards.R;
import com.dcv.spdesigns.dokkancards.presenter.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

/**
 * DokkanCards was
 * Created by Stelios Papamichail on 7/28/2018.
 * <p>
 * This file belongs to the com.dcv.spdesigns.dokkancards.ui package.
 */
public class UsernameDialog extends DialogFragment {

    final String PREFS_NAME = "DokkanCardsPrefs"; // SHAREDPREFS NAME

    @BindView(R.id.editText) EditText mEditText;
    private String username = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // inflate the layout using the dialog themed context
        final Context context = getActivity();
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.username_dialog,null,false);

        ButterKnife.bind(this,view);

        DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                username = mEditText.getText().toString();
                saveUsernameToStorage();
                Log.d("USERNAME",mEditText.getText().toString());
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.MyDialogTheme)
                .setTitle(Html.fromHtml("<font color='#FFFFFF'>Choose a username</font>"))
                .setView(view)
                .setPositiveButton("OK",posListener);
        return builder.create();
    }

    /**
     * Prompts the user for his/her username
     * when the tutorial is done and saves it to the internal storage
     */
    private void saveUsernameToStorage() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Username",username);
        editor.apply();
    }
}
